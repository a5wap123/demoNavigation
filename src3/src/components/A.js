import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import Home from './Home';
import Detail from './Detail';


export const HomeNavigator = StackNavigator({
    StackHome:{
        screen: Home,
       
    },
    StackDetail:{
        screen: Detail,
       
    },
})

