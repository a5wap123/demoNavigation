import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';

import Main from './Main/Main';
import ChangeInfo from './ChangeInfo/ChangeInfo';
import OrderHistory from './OrderHistory/OrderHistory';
import Authentication from './Authentication/Authentication';
export const Navigation = StackNavigator({
    StackHome:{screen: Main,
    navigationOptions:{
            title:'Main',
        }
},
    StackDetail:{screen: ChangeInfo,
    navigationOptions:{
            title:'ChangeInfo',
        }
},
})


