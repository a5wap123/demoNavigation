import React, { Component } from 'react';
import {View,Text,TouchableOpacity} from 'react-native';

export default class Main extends Component {
    render () {
        return (
            <View style={{flex: 1}}>
                <Text> View Main </Text>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate('StackDetail')}}>
                    <Text>Click Here</Text>
                </TouchableOpacity>
            </View>
        );
    }
}